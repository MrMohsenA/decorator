from functools import wraps


def log_trace(filename, trace_message, count_new_line):
    with open(file=f"fixtures/{filename}", mode="a") as file_handler:
        message = "- {0} {1}".format(trace_message, count_new_line * "\n")
        file_handler.write(message)


def trace(func):
    """
    store input and output func in file(trace concept).
    :param func:
    :return:
    """

    @wraps(func)  # maintenance metadata decorate function
    def wrapper(*args, **kwargs):
        log_trace(
            filename=f"{func.__name__}",
            trace_message=f"Trace: Calling {func.__name__}() with {args}, {kwargs}",
            count_new_line=1
        )
        original_result = func(*args, **kwargs)
        log_trace(
            filename=f"{func.__name__}",
            trace_message=f"Trace: {func.__name__}() returned {original_result!r}",
            count_new_line=2
        )
        return original_result

    return wrapper
