import time

from datetime import datetime


def log_time_process(func):
    def wrapper(*args, **kwargs):
        start_time = time.process_time()
        result = func(*args, **kwargs)
        end_time = time.process_time()

        run_time = end_time - start_time

        print(f"Finished {func.__name__!r} in {run_time:.5f} seconds")
        return result

    return wrapper


def log_time(func):
    def wrapper(*args, **kwargs):
        start_time = datetime.now()
        result = func(*args, **kwargs)  # Run main function
        end_time = datetime.now()

        duration = (end_time - start_time).seconds
        format_duration = time.strftime(
            "%H:%M:%S",
            time.gmtime(duration)
        )
        print(f"Total Time: {format_duration}")
        return result

    return wrapper
