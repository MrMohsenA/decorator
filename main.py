from logger import log_time
from trace import trace


# @log_time  # Run recursively print_hi and print many log_time
def print_hi():
    user_input = input("Enter name: ")
    print(f"Hi, {user_input}")

    play_again = input("Do you want to play again? <y/n> ")
    if play_again.lower() == "y":
        print_hi()


@log_time  # print one log_time
def run():
    print_hi()


@trace
def greeting(scores):
    """
    calc average scores
    :param scores:
    :return:
    """
    return round(sum(scores) / len(scores), 2)


if __name__ == '__main__':
    run()
    greeting([14, 15, 13, 11, 10, 19, 5])
    greeting(scores=[14, 15, 13, 11, 10, 19, 6, 15])

    # Run decorate function without execute decorator
    greeting.__wrapped__([12, 15, 17, 19, 20])
